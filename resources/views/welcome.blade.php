@extends('layout.master')

@section('judul')
    Halaman Welcome
@endsection

@section('content')
<h1>SELAMAT DATANG! {{$nama_depan}} {{$nama_belakang}}</h1>
<p><b>Terimakasih telah bergabung di Website Kami. Media Belajar kita bersama!</b></p>
@endsection
